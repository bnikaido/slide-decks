---
marp: true
theme: default
headingDivider: 2
paginate: true
style: |
  section {
    padding: 5%;
    font-size: 150%;
    }
  section.lead h1 {
    text-align: justify;
    font-size: 400%;
  }
  img {
    display: inline;
    margin: 0 auto;
  }
---

# **Conference Summary**

<!-- class: lead -->
<!-- _backgroundColor: #d6d6d6 -->

# Agenda

<!-- _backgroundColor: #d6d6d6 -->

Society of Women Engineers Conference | October 20-22
- State of Women in Engineering
- The Pay Gap
- Microaggression

Texas Conference for Women | November 9-10
- Doing Hybrid Right
- Toxic Workplace
- Sharing Experiences

# SWE | Oct. 20-22

<!-- _backgroundColor: #d6d6d6 -->

# State of Women in Engineering

<!-- class: invert -->
Total Engineering Bachelor's Degrees Awarded to Women by Race/Ethnicity 2020
![A pie chart shows 75% men, 12% White Women, 4% Asian American Women, 3% Hispanic Women, 2% Nonresident Women, 1% Black or African American Women, 1% Multiracial Women, 1% American Indian/Pacific Islander/Unknown Women. Link https://mermaid.live/edit#pako:eNptkEFrAjEQhf9KmLMgtkohty0e6sFSkOIllyGZ7Q6bTJYkixTxvzet7taDOQ3zvXl5vDPY6Ag0DEwqd_G0xYLKiKrPwJ7EgNLqZTNtjh0XUscYbmT1NJEmM4pqAiW2dfiXrCfFG-cBhe0de57Ye5REmR1JucOz-atH26uYVNNe7R_8s5pTj75wQsvoH-H5dCeuRl5-VGVbU-2yR3GUlp_SSzzdrmABVR-QXS3p_OthoHQUyICuo8PUGzByqTocSzx8iwVd0kgLGAeHhbaMXwkD6BZ9rltyXGLaX1v_K__yAxibeiA](./resource/2020-bachelors-mermaid-diagram-2022-11-16-182626.svg)

---


![](./resource/2019-professions.png)

# The Pay Gap


[In 2021, women made $0.82 for every $1.00 made by a man](https://www.payscale.com/research-and-insights/gender-pay-gap/#:~:text=The%20Gender%20Pay%20Gap%20in,the%20same%20as%20last%20year.)
<!-- The controlled gender pay gap is $0.99 for every $1.00, which accounts for all compensable factors such as job title, education, experience, time, etc., but the controlled gender pay gap may not be truly representative since women disproportionately faced unemployment due to COVID-19 impact. -->
![h:500](./resource/2021-pay-gap.png)

# The Pay Gap


<!-- Quiz time! Did you negotiate compensation for your first job?
a: yes
b: no
c: I don't remember
d: I didn't know I could
-->
The only immediate action women can take is negotiate for equitable pay.
Only 7% of women negotiate their first salary while 57% of men do. Why not?
- Gender Expectations
- Cultural Expectations
- Fear of Rejection

Every salary adjustment is negotiable. How to negotiate effectively?
1. Research (glassdoor, payscale, etc.)
    - Be aware of competitive landscape and macroeconomic factors
    - Learn how to measure the value of your skills
    - Define your deal breakers
2. Compare research to your offer
3. Identify alternative forms of compensation
4. Engage confidently and respectfully

# Microaggression


<!-- Quiz time! Do you think "mansplaining" or "manologue" are offensive terms?
a: yes
b: no
c: I've never heard of these terms
-->
[Men speak 75% more than women in meetings](https://www.dailymail.co.uk/sciencetech/article-2205502/The-great-gender-debate-Men-dominate-75-conversation-conference-meetings-study-suggests.html)

- Mansplaining
- Manologue
- Interruptions

How to minimize microaggressions?

- Learn to actively listen
- Ask questions
- Frame perspective

# TxConfWomen | Nov. 9-10

<!-- _class: lead -->
<!-- _backgroundColor: #d6d6d6 -->

# Doing Hybrid Right

<!-- class: invert -->
Remote work is not new, just newly mainstream.
<!-- Quiz time! How do you prefer to work?
a: only remote
b: hybrid
c: only in office
-->
Advantages of remote work:
<!--Video conferencing has existed since the 90s. Hybrid/remote work has decades of research. We just need to leverage it. -->
- Working remote is more productive because of flexibility
- Microaggression is reduced due to digital aids
- Diversity, Equity, and Inclusion is more easily distributed

Challenges of remote/hybrid work:
- Developing emotional trust
<!-- Two types of trust: cognitive and emotional. Cognitive swift trust is assuming trust of colleagues with prior knowledge that they do work and are capable. Emotional trust is founded in belief that we care about each other and their life. In order to navigate, have more intentional conversation. Some people may need-->
- Tech exhaustion
<!-- Our minds and bodies needed the physical and mental break between meetings where we walked around and had time to process. The issue with virtual meetings is that we do not accommodate for these micro-breaks. Schedule meetings to end 10 minutes early, and take short breaks to move around. -->
- Onboarding fresh hires
<!-- People who are starting a job for the first time experience disadvantages with a remote environment because they have more to learn. When hiring someone fresh, consider a 3 month in-person work style for the team to have at least one member spend time in the office with the new hire each day to help them get acclimated with the new job and work environment -->
- Meaningless time in office
<!-- Hybrid is proven to alleviate challenges that some individuals face with remote work; namely in-person interactions with their coworkers. Time spent in the office without your team or people who you work with regularly quickly does become meaningless. Make the time spent in office count by scheduling it with your team to experience any of the benefits that a hybrid work style brings. -->

# Toxic Workplace

<!-- class: invert -->
<!-- Quiz time! Have you ever worked in a toxic environment?
a: yes
b: no
c: I don't know
-->
[Research is showing that The Great Resignation was not about pay...](https://sloanreview.mit.edu/article/toxic-culture-is-driving-the-great-resignation/)
<!-- In fact, pay was not even in the top 10. The primary reason for The Great Resignation is actually toxic culture. -->
![h:500](./resource/Sull_Great_Resignation_Fig_3.webp)

# Toxic Workplace

What makes a workplace toxic?
- Assumptions
- Lack of clarity
- Discrimination
- Microaggressions
- Poor listening skills

Key observations:
1. In a place of privilege, a toxic work culture can be completely unnoticeable
2. Toxic workplaces have a heavy impact on Diversity, Equity, and Inclusion (DEI)
3. "Humans are emotional beings who occasionally think" – Brené Brown

# Toxic Workplace

H-E-B says "be your true, authentic self at work."
Break away from these workplace "taboos":
- Politics
- Religion
- Sexual Orientation
- Gender Identity

By ignoring social issues at work, we enable ignorance of toxic work culture altogether.
Navigating conversation with emotional intelligence:
- Two work check in
- Recognize impactful events
- Bravely share experiences
- Actively listen

# Sharing Experiences


"If we were all the same, we would not have to talk about inclusiveness." – Chimamanda Ngozi Adichie

Sharing experiences at work is beneficial:
- Increases understanding and equitability
- Opens dialogue for new ideas
- Develops trust with colleagues
- Improves inclusive culture

Be aware:
- Sharing experiences takes courage
- Negative reception or rejection promotes toxicity
- Agreement is not required

# Q & A

<!-- _class: lead -->
<!-- _backgroundColor: #d6d6d6 -->
